/*
 * i2s.c
 *
 *  Created on: Mar 18, 2023
 *      Author: ranjitkumar
 */

#include "main.h"
#include "stdbool.h"

// I2S configuration
#define I2S_DATA_WIDTH  16
#define I2S_FRAME_SIZE  32
#define I2S_FRAME_ALIGN false


void i2s_write(uint16_t *data, size_t len) {
    // calculate number of frames
    size_t num_frames = len / (I2S_FRAME_SIZE / I2S_DATA_WIDTH);

    for (size_t i = 0; i < num_frames; i++) {
        // write left channel
        uint16_t left_data = *data++;
        for (int j = 0; j < I2S_DATA_WIDTH; j++) {
            bool bit = (left_data >> (I2S_DATA_WIDTH - 1 - j)) & 0x1;
            HAL_GPIO_WritePin(GPIOB,I2S_SDO, bit);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // write right channel
        uint16_t right_data = *data++;
        for (int j = 0; j < I2S_DATA_WIDTH; j++) {
            bool bit = (right_data >> (I2S_DATA_WIDTH - 1 - j)) & 0x1;
            HAL_GPIO_WritePin(GPIOB,I2S_SDO, bit);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // assert word select
        HAL_GPIO_WritePin(GPIOB,I2S_WS, GPIO_PIN_SET);

        // align frames
        if (I2S_FRAME_ALIGN) {
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // deassert word select
        HAL_GPIO_WritePin(GPIOB,I2S_WS, 0);
    }
}

void i2s_read(uint16_t *data, size_t len) {
    // calculate number of frames
    size_t num_frames = len / (I2S_FRAME_SIZE / I2S_DATA_WIDTH);

    for (size_t i = 0; i < num_frames; i++) {
        uint16_t left_data = 0;
        uint16_t right_data = 0;

        // read left channel
        for (int j = 0; j < I2S_DATA_WIDTH; j++) {
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            bool bit = HAL_GPIO_ReadPin(GPIOB,I2S_SDI);
            left_data |= bit << (I2S_DATA_WIDTH - 1 - j);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // read right channel
        for (int j = 0; j < I2S_DATA_WIDTH; j++) {
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            bool bit = HAL_GPIO_ReadPin(GPIOB,I2S_SDI);
            right_data |= bit << (I2S_DATA_WIDTH - 1 - j);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // discard any remaining data in the frame
        for (int j = 0; j < I2S_FRAME_SIZE - (2 * I2S_DATA_WIDTH); j++) {
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }

        // store the data
        *data++ = left_data;
        *data++ = right_data;

        // align frames
        if (I2S_FRAME_ALIGN) {
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_SET);
            HAL_GPIO_WritePin(GPIOB,I2S_SCK, GPIO_PIN_RESET);
        }
    }
}


