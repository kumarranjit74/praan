/*
 * si1145.c
 *
 *  Created on: Mar 16, 2023
 *      Author: ranjitkumar
 */



#include "si1145.h"


#include "stm32f1xx_hal.h"

I2C_HandleTypeDef hi2c1;


/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void SI1145_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.ClockSpeed = 100000;
  hi2c1.Init.DutyCycle = I2C_DUTYCYCLE_2;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

void SI1145_sensor_Init()
{
    uint8_t tmp_buf[2];
    // Reset the sensor
    tmp_buf[0] = SI1145_REG_MEASRATE_H;
    tmp_buf[1] = SI1145_RESET_CMD;
    HAL_I2C_Master_Transmit(&hi2c1, SI1145_ADDR, tmp_buf, 2, 1000);
    HAL_Delay(10);

    while (HAL_I2C_IsDeviceReady(&hi2c1, SI1145_ADDR, 10, 1000) != HAL_OK);
    tmp_buf[0] = SI1145_REG_MEASRATE_H;
    tmp_buf[1] = 0x01;
    HAL_I2C_Master_Transmit(&hi2c1, SI1145_ADDR, tmp_buf, 2, 1000);
    HAL_Delay(10);


    tmp_buf[0] = SI1145_REG_CHAN_LIST;
    tmp_buf[1] = SI1145_CHAN_LIST_ENUV | SI1145_CHAN_LIST_ENALSIR | SI1145_CHAN_LIST_ENALSVIS;
    HAL_I2C_Master_Transmit(&hi2c1, SI1145_ADDR, tmp_buf, 2, 1000);
    HAL_Delay(10);
    tmp_buf[0] = SI1145_REG_COMMAND;
    tmp_buf[1] = SI1145_ALS_AUTO_CMD;
    HAL_I2C_Master_Transmit(&hi2c1, SI1145_ADDR, tmp_buf, 2, 1000);
    HAL_Delay(10);
}

uint8_t * SI1145_sensor_Read(void) {
    uint8_t tmp_buf[12];

    // Read the sensor data
    tmp_buf[0] = SI1145_REG_ALSVISDATA0;
    HAL_I2C_Master_Transmit(&hi2c1, SI1145_ADDR, tmp_buf, 1, 1000);
    HAL_I2C_Master_Receive(&hi2c1, SI1145_ADDR, tmp_buf, 8, 1000);

    return tmp_buf;
}
