/*
 * pms7003.c
 *
 *  Created on: Mar 16, 2023
 *      Author: ranjitkumar
 */
#include "main.h"
#include "pms7003.h"

UART_HandleTypeDef huart3_airQuality;


/**
  * @brief USART3 Initialization Function
  * @param None
  * @retval None
  */
 void pms7003_uart_Init(void)
{

  /* USER CODE BEGIN USART3_Init 0 */

  /* USER CODE END USART3_Init 0 */

  /* USER CODE BEGIN USART3_Init 1 */

  /* USER CODE END USART3_Init 1 */
  huart3_airQuality.Instance = USART3;
  huart3_airQuality.Init.BaudRate = 115200;
  huart3_airQuality.Init.WordLength = UART_WORDLENGTH_8B;
  huart3_airQuality.Init.StopBits = UART_STOPBITS_1;
  huart3_airQuality.Init.Parity = UART_PARITY_NONE;
  huart3_airQuality.Init.Mode = UART_MODE_TX_RX;
  huart3_airQuality.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart3_airQuality.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart3_airQuality) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART3_Init 2 */

  /* USER CODE END USART3_Init 2 */

}

void pms7003_Init(void)
{
	uint8_t set_up_cmd[7] =  {0x42, 0x4D, 0xE4, 0x00, 0x00, 0x01, 0x73};
	HAL_UART_Transmit(&huart3_airQuality, set_up_cmd,7,1000);
}

void pms7003_ReqData(uint8_t* buffer, uint16_t length)
{
	uint8_t read_data_cmd[7] =  {0x42, 0x4D, 0xE1, 0x00, 0x00, 0x01, 0x71};
    HAL_UART_Transmit(&huart3_airQuality, read_data_cmd,7,1000);
    HAL_Delay(2000);
    HAL_UART_Receive(&huart3_airQuality,buffer, 32,1000);
}
