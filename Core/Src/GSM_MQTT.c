/*
 * GSM_MQTT.c
 *
 *  Created on: Mar 14, 2023
 *      Author: Admin
 */

#include <main.h>
#include <stdint.h>
#include <string.h>

UART_HandleTypeDef huart1_gsm;

static void SendMQTTCmd(char *cmd);
static void GSM_UART1_Init(void);

void GSM_MQTT_Init()
{
   GSM_UART1_Init();

  SendMQTTCmd("ATE0");
  HAL_Delay(2000);
  SendMQTTCmd("AT+CMQTTSTART");
  HAL_Delay(2000);
  SendMQTTCmd("AT+CMQTTACCQ=0,\"elementz/123\""); //Client ID - change this for each client as this need to be unique
  HAL_Delay(2000);
  SendMQTTCmd("AT+CMQTTCONNECT=0,\"tcp://test.mosquitto.org:1883\",90,1"); //MQTT Server Name for connecting this client
  HAL_Delay(2000);
}

void Publish_MqttData()
{

	SendMQTTCmd("AT+CMQTTTOPIC=0,8"); //AT Command for Setting up the Publish Topic Name
	HAL_Delay(1000);
    //SendMQTTCmd(Publish); //Topic Name
    HAL_Delay(1000);
    SendMQTTCmd("AT+CMQTTPAYLOAD=0,1"); //Payload length
    HAL_Delay(1000);
    SendMQTTCmd("a"); //Payload message
    HAL_Delay(1000);
    SendMQTTCmd("AT+CMQTTPUB=0,1,60"); //Acknowledgment
    HAL_Delay(1000);
}



static void GSM_UART1_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1_gsm.Instance = USART1;
  huart1_gsm.Init.BaudRate = 115200;
  huart1_gsm.Init.WordLength = UART_WORDLENGTH_8B;
  huart1_gsm.Init.StopBits = UART_STOPBITS_1;
  huart1_gsm.Init.Parity = UART_PARITY_NONE;
  huart1_gsm.Init.Mode = UART_MODE_TX_RX;
  huart1_gsm.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1_gsm.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart1_gsm) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

static void SendMQTTCmd( char *cmd)
{
	HAL_UART_Transmit(&huart1_gsm, (uint8_t*)cmd, strlen(cmd),20);
}
