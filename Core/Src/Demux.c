/*
 * Demux.c
 *
 *  Created on: Mar 14, 2023
 *      Author: Admin
 */

#include <main.h>

UART_HandleTypeDef huart2_demux;

static void DEMUX_USART2_Init(void);

/**
  * @brief USART2 Initialization Function
  * @param None
  * @retval None
  */
static void DEMUX_USART2_Init(void)
{

  /* USER CODE BEGIN USART2_Init 0 */

  /* USER CODE END USART2_Init 0 */

  /* USER CODE BEGIN USART2_Init 1 */

  /* USER CODE END USART2_Init 1 */
  huart2_demux.Instance = USART2;
  huart2_demux.Init.BaudRate = 9600;
  huart2_demux.Init.WordLength = UART_WORDLENGTH_8B;
  huart2_demux.Init.StopBits = UART_STOPBITS_1;
  huart2_demux.Init.Parity = UART_PARITY_NONE;
  huart2_demux.Init.Mode = UART_MODE_TX_RX;
  huart2_demux.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart2_demux.Init.OverSampling = UART_OVERSAMPLING_16;
  if (HAL_UART_Init(&huart2_demux) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART2_Init 2 */

  /* USER CODE END USART2_Init 2 */

}

void Demux_GetNo2Reading(uint8_t *data)
{
	  HAL_GPIO_WritePin(GPIOA, DEMUX_SELECT_PIN_A|DEMUX_SELECT_PIN_B, GPIO_PIN_SET);
	  HAL_UART_Transmit(&huart2_demux, 'e',1, 200);
	  HAL_Delay(2000);
	  HAL_UART_Receive(&huart2_demux, data,15, 200);
	  HAL_Delay(2000);
}

void Demux_GetSo2Reading(uint8_t *data)
{
	  HAL_GPIO_WritePin(GPIOA, DEMUX_SELECT_PIN_A|DEMUX_SELECT_PIN_B, GPIO_PIN_RESET);
	  HAL_UART_Transmit(&huart2_demux, 'e',1, 200);
	  HAL_Delay(2000);
	  HAL_UART_Receive(&huart2_demux, data,15, 200);
	  HAL_Delay(2000);
}
