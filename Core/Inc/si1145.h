/*
 * si1145.h
 *
 *  Created on: Mar 16, 2023
 *      Author: ranjitkumar
 */

#ifndef INC_SI1145_H_
#define INC_SI1145_H_

#define SI1145_ADDR                  0x60
#define SI1145_RESET_CMD             0x01
#define SI1145_REG_MEASRATE_H        0x84
#define SI1145_REG_CHAN_LIST         0x01
#define SI1145_CHAN_LIST_ENUV        0x80
#define SI1145_CHAN_LIST_ENALSIR     0x20
#define SI1145_CHAN_LIST_ENALSVIS    0x10
#define SI1145_REG_COMMAND           0x18
#define SI1145_REG_ALSVISDATA0       0x22
#define SI1145_ALS_AUTO_CMD          0x0E
#define SI1145_REG_COMMAND           0x18



#endif /* INC_SI1145_H_ */
